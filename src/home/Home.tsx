import React from 'react';
import { AlertsDelegate, AlertsTab } from '../alerts/AlertsTab';
import { EditAlertDelegate } from '../alerts/EditAlert';
import { AppState } from '../model/watchlist';
import { AddSymbolDelegate } from '../stocks/AddSymbol';
import { StocksTab } from '../stocks/StocksTab';
import { StocksTableDelegate } from '../stocks/StocksTable';
import { Nav, NavDelegate } from './Nav';

import './Home.less';

export type HomeDelegate = NavDelegate & StocksTableDelegate & AddSymbolDelegate & EditAlertDelegate & AlertsDelegate & EditAlertDelegate;

export function Home(props: { data: AppState, delegate: HomeDelegate }) {
    return <div className="home">
        <div className="home-title">Stocks Viewer</div>
        <Nav selected={props.data.tab} delegate={props.delegate}/>
        {props.data.tab === 'stocks' && <StocksTab data={props.data} delegate={props.delegate}/>}
        {props.data.tab === 'alerts' && <AlertsTab alerts={props.data.alerts} delegate={props.delegate}/>}
    </div>;
}
