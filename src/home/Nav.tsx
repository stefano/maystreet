import React from 'react';
import { Tab } from '../model/watchlist';

import './Nav.less';

export interface NavDelegate {
    onOpenStocks(): void;
    onOpenAlerts(): void;
}

export function Nav(props: { selected: Tab, delegate: NavDelegate }) {
    return <div className="nav">
        <div className={props.selected === 'stocks' ? 'selected' : ''} onClick={props.delegate.onOpenStocks}>Stocks</div>
        <div className={props.selected === 'alerts' ? 'selected' : ''} onClick={props.delegate.onOpenAlerts}>Alerts</div>
    </div>;
}
