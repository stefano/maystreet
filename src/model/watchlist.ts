import { prices, SymData } from '../api';

export type Tab = 'stocks' | 'alerts';

export interface AppState {
    tab: Tab;
    editing: WatchlistAlertEdit | null;
    selectedSymbol: string;
    watchlist: WatchlistItem[];
    alerts: Alert[];
    alreadyAlerted: string[];
    symbols: string[];
}

export interface WatchlistAlertEdit {
    item: WatchlistItem;
    tmpEnabled: boolean;
    tmpLimit: number | null;
    tmpLimitText: string;
    tmpLimitError: string;
}

export interface Alert {
    sym: string;
    time: Date;
    movement: number;
}

export interface WatchlistItem {
    sym: string;
    bid: number;
    ask: number;
    vol: number;
    alertsEnabled: boolean;
    alertsLimit: number | null;
}

export const INITIAL_STATE: AppState = {
    tab: 'stocks',
    editing: null,
    selectedSymbol: '',
    watchlist: [],
    alerts: [],
    alreadyAlerted: [],
    symbols: [],
};

export function setTab(state: AppState, tab: Tab): AppState {
    return { ...state, tab };
}

export function setSymbols(state: AppState, syms: string[]): AppState {
    return { ...state, symbols: syms };
}

export function setSelectSymbol(state: AppState, sym: string): AppState {
    return { ...state, selectedSymbol: sym };
}

export function addSymbol(state: AppState): AppState {
    if (!state.selectedSymbol || state.watchlist.filter(i => i.sym === state.selectedSymbol).length > 0) {
        return state;
    }

    const item: WatchlistItem = {
        sym: state.selectedSymbol,
        bid: 0,
        ask: 0,
        vol: 0,
        alertsEnabled: false,
        alertsLimit: null
    };

    return { ...state, selectedSymbol: '', watchlist: state.watchlist.concat([item]) };
}

export function toggleItemAlert(state: AppState, sym: string): AppState {
    const watchlist = state.watchlist.map(i => i.sym === sym ? {
        ...i,
        alertsEnabled: !i.alertsEnabled,
    } : i);

    return { ...state, watchlist };
}

export function removeSymbol(state: AppState, sym: string): AppState {
    return { ...state, watchlist: state.watchlist.filter(i => i.sym !== sym) };
}

export function editAlert(state: AppState, item: WatchlistItem): AppState {
    return {
        ...state,
        editing: {
            item,
            tmpEnabled: item.alertsEnabled,
            tmpLimit: item.alertsLimit,
            tmpLimitText: item.alertsLimit?.toString() ?? '',
            tmpLimitError: ''
        }
    };
}

export function setAlertTmpLimit(state: AppState, limit: string): AppState {
    if (!state.editing) {
        return state;
    }

    const value = limit.trim() === '' ? null : parseFloat(limit);
    const error = value !== null && isNaN(value) ? 'Invalid number' : '';

    return { ...state, editing: { ...state.editing, tmpLimit: value, tmpLimitText: limit, tmpLimitError: error } };
}

export function setAlertTmpEnabled(state: AppState, enabled: boolean): AppState {
    if (!state.editing) {
        return state;
    }

    return { ...state, editing: { ...state.editing, tmpEnabled: enabled } };
}

export function saveAlert(state: AppState): AppState {
    if (!state.editing) {
        return state;
    }

    const watchlist = state.watchlist.map(i => i.sym === state.editing?.item?.sym ? {
        ...i,
        alertsEnabled: state.editing.tmpEnabled,
        alertsLimit: state.editing.tmpLimit
    } : i);

    return {
        ...state,
        editing: null,
        watchlist,
        alreadyAlerted: state.alreadyAlerted.filter(s => s !== state.editing?.item?.sym)
    };
}

export function removeAlert(state: AppState, alert: Alert): AppState {
    return {
        ...state,
        alerts: state.alerts.filter(a => a !== alert),
        alreadyAlerted: state.alreadyAlerted.filter(s => s !== alert.sym)
    };
}

let refreshCounter = 0;

export async function refreshPrices(state: AppState): Promise<SymData[] | null> {
    if (state.watchlist.length === 0) {
        return null;
    }

    const counter = ++refreshCounter;
    const newPrices = await prices(state.watchlist.map(i => i.sym));
    if (counter !== refreshCounter) {
        // request took too long, another one is already in flight, bail out
        return null;
    } else {
        return newPrices;
    }
}

export function updatePrices(state: AppState, newPrices: SymData[]): AppState {
    const bySym = new Map<string, SymData>();
    for (const price of newPrices) {
        bySym.set(price.symbol, price);
    }

    const watchlist = state.watchlist.map(i => {
        const price = bySym.get(i.sym);
        if (!price) {
            // somehow missing, keep old one
            return i;
        }

        return {
            ...i,
            bid: price.bid,
            ask: price.ask,
            vol: price.lastVol
        };
    });

    const newAlerts: Alert[] = [];
    for (const item of state.watchlist) {
        const price = bySym.get(item.sym);
        if (!price) {
            continue;
        }

        if (state.alreadyAlerted.indexOf(item.sym) < 0 && item.alertsEnabled && item.alertsLimit !== null) {
            const movement = price.bid / item.bid * 100 - 100;
            if (Math.abs(movement) >= Math.abs(item.alertsLimit)) {
                newAlerts.push({ sym: item.sym, time: new Date(), movement });
            }
        }
    }

    return {
        ...state,
        // send user to alert tab if there's a new alert and he's not in the middle of editing something
        tab: newAlerts.length > 0 && state.editing === null ? 'alerts' : state.tab,
        watchlist,
        alerts: newAlerts.concat(state.alerts),
        alreadyAlerted: newAlerts.map(a => a.sym).concat(state.alreadyAlerted)
    };
}
