const BASE_URL = 'http://localhost:3000/'

export async function symbols(): Promise<string[]> {
    const response = await fetch(`${BASE_URL}static/tickers`);
    return response.json();
}

export interface SymData {
    symbol: string;
    bid: number;
    ask: number;
    lastVol: number;
    open: number;
}

export async function prices(syms: string[]): Promise<SymData[]> {
    const response = await fetch(`${BASE_URL}prices/${syms.join(',')}`);
    return response.json();
}
