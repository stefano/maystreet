import React from 'react';
import { EditAlert, EditAlertDelegate } from '../alerts/EditAlert';
import { AppState } from '../model/watchlist';
import { AddSymbol, AddSymbolDelegate } from './AddSymbol';
import { StocksTable, StocksTableDelegate } from './StocksTable';

export function StocksTab(props: { data: AppState, delegate: StocksTableDelegate & AddSymbolDelegate & EditAlertDelegate }) {
    return <div>
        {props.data.watchlist.length > 0 && <StocksTable watchlist={props.data.watchlist} delegate={props.delegate}/>}
        <AddSymbol data={props.data} delegate={props.delegate}/>
        {props.data.editing && <EditAlert data={props.data.editing} delegate={props.delegate}/>}
    </div>;
}
