import React from 'react';
import { AppState } from '../model/watchlist';

import './AddSymbol.less';

export interface AddSymbolDelegate {
    onSymbolSelected(sym: string): void;
    onAddSymbol(): void;
}

export function AddSymbol(props: { data: AppState, delegate: AddSymbolDelegate }) {
    return <div className="add-symbol">
        <div>Add symbol:</div>
        <select value={props.data.selectedSymbol} onChange={(evt) => props.delegate.onSymbolSelected(evt.target.value)}>
            <option value="">Select</option>
            {props.data.symbols.map(sym => <option key={sym} value={sym}>{sym}</option>)}
        </select>
        <button onClick={props.delegate.onAddSymbol}>Add</button>
    </div>;
}
