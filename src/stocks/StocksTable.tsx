import React from "react";
import { WatchlistItem } from "../model/watchlist";

import './StocksTable.less';

export interface StocksTableDelegate {
    onToggleAlert(sym: string): void;
    onEditAlert(sym: string): void;
    onRemoveSymbol(sym: string): void;
}

export function StocksTable(props: { watchlist: WatchlistItem[], delegate: StocksTableDelegate }) {
    return <table className="stocks-table">
        <thead>
            <tr>
                <th>Stock</th>
                <th>Bid</th>
                <th>Ask</th>
                <th>Vol.</th>
                <th>Alerts</th>
                <th>Options</th>
            </tr>
        </thead>
        <tbody>
            {props.watchlist.map(row => <tr key={row.sym}>
                <td>{row.sym}</td>
                <td>{row.bid.toLocaleString(undefined, { maximumFractionDigits: 2, minimumFractionDigits: 2 })}</td>
                <td>{row.ask.toLocaleString(undefined, { maximumFractionDigits: 2, minimumFractionDigits: 2 })}</td>
                <td>{row.vol.toLocaleString(undefined, { maximumFractionDigits: 0 })}</td>
                <td>
                    <input type="checkbox" checked={row.alertsEnabled} onChange={() => props.delegate.onToggleAlert(row.sym)}></input>
                </td>
                <td>
                    <a onClick={() => props.delegate.onEditAlert(row.sym)}>Alerts</a>
                    <a className="destroy" onClick={() => props.delegate.onRemoveSymbol(row.sym)}>&#x2715;</a>
                </td>
            </tr>)}
        </tbody>
    </table>;
}
