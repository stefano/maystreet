import React from 'react';
import ReactDOM from 'react-dom';
import { App, AppController } from './AppController';

document.addEventListener('DOMContentLoaded', () => {
    const controller = new AppController();
    ReactDOM.render(<App controller={controller}/>, document.getElementById('content'));
});
