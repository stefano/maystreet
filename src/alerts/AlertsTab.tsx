import React from 'react';
import { Alert } from '../model/watchlist';

import './AlertsTab.less';

export interface AlertsDelegate {
    onAlertRemove(alert: Alert): void;
}

export function AlertsTab(props: { alerts: Alert[], delegate: AlertsDelegate }) {
    if (props.alerts.length === 0) {
        return <div>There are no alerts.</div>
    }

    return <div className="alerts">
        {props.alerts.map(alert => <div key={`${alert.sym}-${alert.time.getTime()}`}>
            <div className="title-row">
                <div>{alert.sym} @ {alert.time.toLocaleTimeString()}</div>
                <div className="remove-alert" onClick={() => props.delegate.onAlertRemove(alert)}>&#x2715;</div>
            </div>
            <div>
                <span>{alert.sym}</span>'s BID price moved <span className={alert.movement > 0 ? 'up' : 'down' }>{alert.movement.toLocaleString(undefined, { minimumFractionDigits: 0, maximumFractionDigits: 2 })}%</span> from the opening price at {alert.time.toLocaleTimeString()} today.
            </div>
        </div>)}
    </div>;
}
