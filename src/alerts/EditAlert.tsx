import React from 'react';
import { WatchlistAlertEdit } from '../model/watchlist';

import './EditAlert.less';

export interface EditAlertDelegate {
    onEditAlertToggleEnable(): void;
    onEditAlertLimitChanged(value: string): void;
    onEditAlertConfirm(): void;
}

export interface AlertTrigger {
    enabled: boolean;
    limit: number;
}

export function EditAlert(props: { data: WatchlistAlertEdit, delegate: EditAlertDelegate }) {
    return <div className="overlay">
        <div>
            <div className="title">Edit alert</div>
            <div>
                <label>
                    <input type="checkbox" checked={props.data.tmpEnabled} onChange={() => props.delegate.onEditAlertToggleEnable()}/>
                    <span>Alert me when the stock price deviates from the opening price by:</span>
                </label>
                <div className="limit-container">
                    <div><input type="text" value={props.data.tmpLimitText} onChange={(evt) => props.delegate.onEditAlertLimitChanged(evt.target.value)}/> %</div>
                    {props.data.tmpLimitError && <div className="error">{props.data.tmpLimitError}</div>}
                </div>
            </div>

            <div className="edit-alert-actions">
                <button onClick={() => props.delegate.onEditAlertConfirm()}>OK</button>
            </div>
        </div>
    </div>;
}
