import React from 'react';
import { useState } from 'react';
import { symbols } from './api';
import { Home, HomeDelegate } from './home/Home';
import { addSymbol, Alert, AppState, editAlert, INITIAL_STATE, refreshPrices, removeAlert, removeSymbol, saveAlert, setAlertTmpEnabled, setAlertTmpLimit, setSelectSymbol as setSelectedSymbol, setSymbols, setTab, toggleItemAlert, updatePrices } from './model/watchlist';

const REFRESH_INTERVAL_MS = 500;

export class AppController implements HomeDelegate {
    state: AppState = INITIAL_STATE;
    updateState: ((state: AppState) => void) | null = null;

    constructor() {
        symbols().then(syms => this.update(setSymbols(this.state, syms)));
        setInterval(async () => {
            const prices = await refreshPrices(this.state);
            if (prices != null) {
                this.update(updatePrices(this.state, prices));
            }
        }, REFRESH_INTERVAL_MS);
    }

    private update(state: AppState) {
        this.state = state;
        this.updateState?.(state);
    }

    onOpenStocks = () => {
        this.update(setTab(this.state, 'stocks'));
    }

    onOpenAlerts = () => {
        this.update(setTab(this.state, 'alerts'));
    }

    onToggleAlert = (sym: string) => {
       this.update(toggleItemAlert(this.state, sym));
    }

    onEditAlert = (sym: string) => {
        const item = this.state.watchlist.filter(i => i.sym === sym)[0];
        if (item) {
            this.update(editAlert(this.state, item));
        }
    }

    onRemoveSymbol = (sym: string) => {
        this.update(removeSymbol(this.state, sym));
    }

    onSymbolSelected = (sym: string) => {
        this.update(setSelectedSymbol(this.state, sym));
    }

    onAddSymbol = () => {
        this.update(addSymbol(this.state));
    }

    onEditAlertToggleEnable = () => {
        this.update(setAlertTmpEnabled(this.state, !this.state.editing?.tmpEnabled));
    }

    onEditAlertLimitChanged = (value: string) => {
        this.update(setAlertTmpLimit(this.state, value));
    }

    onEditAlertConfirm = () => {
        this.update(saveAlert(this.state));
    }

    onAlertRemove = (alert: Alert) => {
        this.update(removeAlert(this.state, alert));
    }
}

export function App(props: { controller: AppController }) {
    const [state, updateState] = useState(props.controller.state);
    props.controller.updateState = updateState;

    return <Home data={state} delegate={props.controller}/>;
}
